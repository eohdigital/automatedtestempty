package com.digital.platoon.features;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

@Data
@ContextConfiguration(classes = Config.class)
public class BaseFeature {

  @Value("${headless}")
  private String headless;

  @Value("${enabled.browsers}")
  private String enabledBrowsersString;

  private List<RemoteWebDriver> enabledDrivers;

  public BaseFeature() {
    enabledDrivers = new ArrayList();
  }

  public List<RemoteWebDriver> initBrowsers() {
    String enabledBrowsersStrings[] = enabledBrowsersString.split(",");
    for (int i = 0; i < enabledBrowsersStrings.length; i++) {
      switch (enabledBrowsersStrings[i]) {
        case "chrome":
          System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver");
          enabledDrivers.add(new ChromeDriver(new ChromeOptions().setHeadless(Boolean.parseBoolean(headless))));
          break;

        case "gecko":
          System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver");
          enabledDrivers.add(new FirefoxDriver(new FirefoxOptions().setHeadless(Boolean.parseBoolean(headless))));
          break;

        default:
          System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver");
          enabledDrivers.add(new ChromeDriver(new ChromeOptions().setHeadless(Boolean.parseBoolean(headless))));
      }
    }
    return enabledDrivers;
  }

  public void closeBrowsers() {
    for (RemoteWebDriver driver : getEnabledDrivers()) {
      driver.close();
    }
    setEnabledDrivers(new ArrayList());
  }
}
